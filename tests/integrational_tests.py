from sqlalchemy.engine.create import create_engine
from sqlalchemy.orm.session import sessionmaker
from fastapi.testclient import TestClient

from app.controllers.routers import get_db
from app.logic.database import Base
from app.main import app
from app.schemas import schemas

SQLALCHEMY_DATABASE_URL = "sqlite:///./test.db"
engine = create_engine(
    SQLALCHEMY_DATABASE_URL, connect_args={"check_same_thread": False}
)
TestingSessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)
Base.metadata.create_all(bind=engine)


def override_get_db():
    db = TestingSessionLocal()
    try:
        yield db
    finally:
        db.close()


def clean_db():
    db = TestingSessionLocal()
    try:
        db.query(schemas.User).delete()
        db.commit()
        db.query(schemas.Internship).delete()
        db.commit()
    finally:
        db.close()


app.dependency_overrides[get_db] = override_get_db
client = TestClient(app)
now = '2021-09-26T16:29:06.811823'


def test_add_internships():
    clean_db()
    client.post("/internships/new", json={
        "name": "test",
        "description": "test 1",
        "updated_at": now,
        "application_num": 0,
        "is_open": True
    })
    client.post("/internships/new", json={
        "name": "new test",
        "description": "test 2",
        "updated_at": now,
        "application_num": 0,
        "is_open": True
    })
    response = client.get("/internships/")
    assert response.status_code == 200
    assert response.json() == [{
        "name": "test",
        "description": "test 1",
        "updated_at": now,
        "application_num": 0,
        "is_open": True
    }, {
        "name": "new test",
        "description": "test 2",
        "updated_at": now,
        "application_num": 0,
        "is_open": True
    }, ]
    clean_db()


def test_add_duplicate_internships():
    clean_db()
    client.post("/internships/new", json={
        "name": "test",
        "description": "test 1",
        "updated_at": now,
        "application_num": 0,
        "is_open": True
    })
    client.post("/internships/new", json={
        "name": "test",
        "description": "test 1",
        "updated_at": now,
        "application_num": 0,
        "is_open": True
    })
    response = client.get("/internships/")
    assert response.status_code == 200
    assert response.json() == [{
        "name": "test",
        "description": "test 1",
        "updated_at": now,
        "application_num": 0,
        "is_open": True
    }]
    clean_db()


def test_add_user():
    client.post("/users/new", json={
        "name": "test"
    })
    client.post("/users/new", json={
        "name": "new test"
    })
    response = client.get("/users/")
    assert response.status_code == 200
    assert response.json() == [{
        "name": "test",
    },
        {
            "name": "new test",
        },
    ]
    clean_db()


def test_add_duplicates_user():
    client.post("/users/new", json={
        "name": "test"
    })
    client.post("/users/new", json={
        "name": "test"
    })
    response = client.get("/users/")
    assert response.status_code == 200
    assert response.json() == [{
        "name": "test",
    }
    ]
    clean_db()
