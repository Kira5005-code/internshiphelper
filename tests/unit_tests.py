from sqlalchemy.engine.create import create_engine
from sqlalchemy.orm.session import sessionmaker
from fastapi.testclient import TestClient

from app.controllers.routers import get_db
from app.logic.database import Base
from app.logic import utils
from app.main import app
from app.models import models
from app.schemas import schemas

SQLALCHEMY_DATABASE_URL = "sqlite:///./test.db"
engine = create_engine(
    SQLALCHEMY_DATABASE_URL, connect_args={"check_same_thread": False}
)
TestingSessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)
Base.metadata.create_all(bind=engine)


def override_get_db():
    db = TestingSessionLocal()
    try:
        yield db
    finally:
        db.close()


def clean_db():
    db = TestingSessionLocal()
    try:
        db.query(schemas.User).delete()
        db.commit()
        db.query(schemas.Internship).delete()
        db.commit()
    finally:
        db.close()


app.dependency_overrides[get_db] = override_get_db
client = TestClient(app)
now = '2021-09-26T16:29:06.811823'


def test_create_internship():
    internship = models.Internship(name="first", description="test 1", updated_at=now, application_num=0, is_open=True)
    res_internship = utils.create_internship(db=TestingSessionLocal(), internship=internship)
    assert res_internship.id == 1
    assert res_internship.name == "first"
    assert res_internship.description == "test 1"
    assert res_internship.application_num == 0
    assert res_internship.is_open


def test_get_internship_by_name():
    internship = utils.get_internship_by_name(db=TestingSessionLocal(), name="first")
    assert internship.id == 1
    assert internship.name == "first"
    assert internship.description == "test 1"
    assert internship.application_num == 0
    assert internship.is_open


def test_get_internship_by_id():
    internship = utils.get_internship(db=TestingSessionLocal(), internship_id=1)
    assert internship.id == 1
    assert internship.name == "first"
    assert internship.description == "test 1"
    assert internship.application_num == 0
    assert internship.is_open


def test_create_user():
    user = models.User(name="first")
    res_user = utils.create_user(db=TestingSessionLocal(), user=user)
    assert res_user.id == 1
    assert res_user.name == "first"


def test_get_user_by_name():
    user = utils.get_user_by_name(db=TestingSessionLocal(), name="first")
    assert user.id == 1
    assert user.name == "first"


def test_get_user_by_id():
    user = utils.get_user(db=TestingSessionLocal(), user_id=1)
    assert user.id == 1
    assert user.name == "first"


def test_add_internship():
    clean_db()
    response = client.post("/internships/new", json={
        "name": "test",
        "description": "test 1",
        "updated_at": now,
        "application_num": 0,
        "is_open": True
    })
    assert response.status_code == 200
    assert response.json() == {
        "name": "test",
        "description": "test 1",
        "updated_at": now,
        "application_num": 0,
        "is_open": True
    }


def test_add_duplicate_internship():
    response = client.post("/internships/new", json={
        "name": "test",
        "description": "test 1",
        "updated_at": now,
        "application_num": 0,
        "is_open": True
    })
    assert response.status_code == 400


def test_add_user():
    response = client.post("/users/new", json={
        "name": "test"
    })
    assert response.status_code == 200
    assert response.json() == {
        "name": "test",
    }


def test_add_duplicate_user():
    response = client.post("/users/new", json={
        "name": "test"
    })
    assert response.status_code == 400
