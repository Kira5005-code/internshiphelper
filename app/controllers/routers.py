from typing import List
from fastapi import APIRouter
from fastapi.exceptions import HTTPException
from fastapi.param_functions import Depends
from sqlalchemy.orm.session import Session

from app.models import models
from app.logic import utils, database

router = APIRouter()
database.Base.metadata.create_all(bind=database.engine)


def get_db():
    db = database.SessionLocal()
    try:
        yield db
    finally:
        db.close()


@router.post("/internships/new", response_model=models.Internship)
def create_new_internship(internship: models.Internship, db: Session = Depends(get_db)):
    db_internship = utils.get_internship_by_name(db, name=internship.name)
    if db_internship:
        raise HTTPException(status_code=400, detail="Internship already exist")
    internship = utils.create_internship(db=db, internship=internship)
    return models.Internship(name=internship.name, description=internship.description, updated_at=internship.updated_at,
                             is_open=internship.is_open, application_num=internship.application_num)


@router.get("/internships/", response_model=List[models.Internship])
def get_internships(skip: int = 0, limit: int = 100, db: Session = Depends(get_db)):
    internships = list(map(lambda x: models.Internship(name=x.name, description=x.description, updated_at=x.updated_at,
                                                       is_open=x.is_open, application_num=x.application_num),
                           utils.get_internships(db, skip=skip, limit=limit)))
    return internships


@router.post("/users/new", response_model=models.User)
def create_new_user(user: models.User, db: Session = Depends(get_db)):
    db_user = utils.get_user_by_name(db, name=user.name)
    if db_user:
        raise HTTPException(status_code=400, detail="Name already registered")
    user = utils.create_user(db=db, user=user)
    return models.User(name=user.name)


@router.get("/users/", response_model=List[models.User])
def get_users(skip: int = 0, limit: int = 100, db: Session = Depends(get_db)):
    users = list(map(lambda x: models.User(name=x.name), utils.get_users(db, skip=skip, limit=limit)))
    return users
